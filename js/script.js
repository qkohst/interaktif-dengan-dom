
// data barang 
var items = [
  ['001', 'Keyboard Logitek', 60000, 'Keyboard yang mantap untuk kantoran', 'logitek.jpg'],
  ['002', 'Keyboard MSI', 300000, 'Keyboard gaming MSI mekanik', 'msi.jpg'],
  ['003', 'Mouse Genius', 50000, 'Mouse Genius biar lebih pinter', 'genius.jpeg'],
  ['004', 'Mouse Jerry', 30000, 'Mouse yang disukai kucing', 'jerry.jpg']
]

// release 0 menampilkan data ke card
var listBarang = document.getElementById("listBarang")
function printItems(array) {
  var dataListBarang = "";
  for (var i = 0; i < array.length; i++) {
    var currentItem = array[i]
    dataListBarang += `<div class="card mt-4" style="width: 18rem;">
                        <img src="img/${currentItem[4]}" class="card-img-top" alt="...">
                        <div class="card-body">
                            <h6 class="card-title" id="itemName">${currentItem[1]}</h6>
                            <p class="card-text" id="itemDesc">${currentItem[3]}</p>
                            <p class="card-text">Rp ${currentItem[2]}</p>
                            <a href="javascript:;"  onclick="_addToCart();" class="btn btn-sm btn-primary" id="addCart">Add to Cart</a>
                        </div>
                      </div>`
  }
  listBarang.innerHTML = dataListBarang
}
printItems(items)

// release 1 search data by name dataItemBarang 
function filter(kataKunci) {
  var filteredItems = []
  for (var j = 0; j < items.length; j++) {
    var item = items[j]
    var namaItem = item[1]
    var isMatch = namaItem.toLowerCase().includes(kataKunci.toLowerCase())
    if (isMatch == true) {
      filteredItems.push(item)
    }
  }
  return filteredItems
}

// release 1 by using submit event
var formSearch = document.getElementById("formItem")
formSearch.addEventListener("submit", function (event) {
  event.preventDefault()
  var keyword = document.getElementById("keyword").value

  var terfilter = filter(keyword)
  printItems(terfilter)
})

// release 1 by using input key form 
var inputSearch = document.getElementById("keyword")
inputSearch.addEventListener("keyup", function (e) {
  var value = event.target.value
  var filterDenganKeyup = filter(value)
  printItems(filterDenganKeyup)
})



// olah shop cart 
var $cartContainer = $(".cart-container"),
  $cartContent = $(".cart-content"),
  $closeCart = $("#closeCart"),
  $cartSmall = $(".cart-small"),
  $cartSmallNum = $(".cart-small-num");
$cartSmall.on("click", function () {
  $("body").addClass("cart-open");

  $cartContainer.addClass("opened");
});

$closeCart.on("click", function () {
  $("body").removeClass("cart-open");
  $cartContainer.removeClass("opened");
});
function _addToCart() {
  var productItem = '<div class="cart-item">\
    <img src="sampel.jpg" />\
    <div class="text">\
      <h6 class="title">\
        <span>Judul Produk </span>\
        <span>IDR. 100,000</span>\
      </h6>\
      <div class="action">\
        <a href="javascript:;" class="trash-item text-muted">Hapus</a>\
      </div>\
    </div>\
  </div>';
  $cartContent.append(productItem);
  _cartTotalItem();

}
// Tambah Angka di Keranjang Belanja
function _cartTotalItem() {

  var countItem = $cartContent.children().length;
  $cartSmallNum.text(countItem);

}
